precise32-postgis-laravel
=========================
A Vagrant PostGIS and Laravel setup using a custom box precise32-postgis.  To create this box see https://github.com/mikeblakeney/precise32-postgis

### Setup
1.  Use composer (http://getcomposer.org) to create a clean Laravel project

  `composer create-project laravel/laravel yourprojectname`

2. Clone the project in a separate directory 
   `git clone https://github.com/mikeblakeney/precise32-postgis-laravel`

3. Copy the contents of the clone into your project directory
   `cp precise-postgis-laravel/* <path to your project>/.`

4. Make any changes to the Vagrantfile for your particular VM setup

5. Startup the VM

  `vagrant up`

6. If you would like to run the DB setup script just run

  `./laravel-db-setup.php`

7.  To add a new osm data run:

  `./load-osm-data.sh`

8.  Navigate to your localhost (default is http://localhost:8080 ).  You should see laravel and PostGIS playing nicely with each other.

### Remarks
-  You will probably need to add a port number to the localhost per your Vagrantfile setup.  The default is set to 8080

-  You can find osm.pbf map data to load into your db at
   http://download.geofabrik.de/

## References:
   -  For information on installing and using Vagrant refer to:
   
   http://www.vagrantup.com

   -  For information on installing and using Laravel refer to:
   
   http://www.laravel.com

   -  For information on OpenStreetMaps visit
   
   http://www.openstreetmap.org/
   
   http://switch2osm.org/

