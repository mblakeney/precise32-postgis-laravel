#!/usr/bin/env bash

# Inspired by Laracasts Lesson Bash Bootstrap
# https://laracasts.com/lessons/bash-bootstraps

echo -n "What is the database Laravel database name? "
read -e DATABASE

echo --- Creating MySQL Database ---
mysql -uroot -p -e "CREATE DATABASE $DATABASE"

echo --- Updating database configuration file ---
sed -i "s/'database' => 'database'/'database' => '$DATABASE'/" app/config/database.php
sed -i "s/'password' => ''/'password' => 'root'/" app/config/database.php

