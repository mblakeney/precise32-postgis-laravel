#!/usr/bin/env bash

# Script to load geo data into your postgis db
# This script is also inspired from the web page:
# http://switch2osm.org/serving-tiles/building-a-tile-server-from-packages/

# Assuming bootstrap.sh has been run and postgis is properly configured

echo -n "What OSM Geodata file would you like to use (enter url)? "
read -e GEOFILE
curl "$GEOFILE" > tiledata.osm.pbf

# add it to the db
# modify these settings according to your server 
osm2pgsql --slim -C 1000 --number-processes 4 tiledata.osm.pbf --cache-strategy sparse


sudo touch /var/lib/mod_tile/planet-import-complete

# restart rendering engine
sudo /etc/init.d/renderd restart
